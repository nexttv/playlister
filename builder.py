# -*- coding: utf-8 -*-

import time
import re
import os.path
from mako.template import Template
import collections
import xml.etree.ElementTree as ET

class HLSPlaylistBuilder(object):

    time_scale = 10000000
    default_live_delay = 10

    def __init__(self, storage):
        self.storage = storage

    def on_get(self, req, resp, resource_id):

        adapt = req.get_param_as_bool('adapt') or False

        if adapt:
            resp.body = self.adapt_pl(resource_id)
        else:
            quality = req.get_param_as_int('q', required=True)
            resp.body = self.vod(resource_id, quality)

        resp.content_type = "application/x-mpegURL"

    def adapt_pl(self, resource_id):

        tmpl_path = os.path.dirname(os.path.realpath(__file__)) + '/templates/hlsv3_adapt.mako'
        tmpl = open(tmpl_path).read()

        segs = self.storage.get_raw_segments(resource_id, start_time=0, limit=1)

        streams = self.prepare_streams(resource_id, segs)

        r = Template(tmpl).render(streams=streams)
        return r

    def prepare_streams(self, resource_id, raw_segments):
        slice_ = self.storage.get_slice(resource_id, raw_segments[0]['s'])

        streams = collections.defaultdict(list)
        slave_playlists = collections.defaultdict(dict)

        for t in slice_['track_list']:
            # tid = t['track_id']
            sorted_qlist = sorted(t['quality_list'], key=lambda x: x['bitrate']['average'])

            for idx, q in enumerate(sorted_qlist):
                streams[idx].append(q)

        assert len(streams) == len(slice_['track_list'][0]['quality_list'])

        for i, s in streams.items():
            avg_bitrate = sum([x['bitrate']['average'] for x in s])
            slave_playlists[i]['bitrate'] = avg_bitrate
            slave_playlists[i]['url'] = "{}.m3u8?q={}".format(resource_id, i)

        return slave_playlists

    def _make_qid_tp_sid(self, slice_):

        qid_to_sid = {}

        for t in slice_['track_list']:
            # tid = t['track_id']
            sorted_qlist = sorted(t['quality_list'], key=lambda x: x['bitrate']['average'])

            for idx, q in enumerate(sorted_qlist):
                qid_to_sid[q['quality_id']] = idx

        return qid_to_sid

    def _get_init_seg(self, init_segs, resource_id, track_id, quality_id, slice_id):

        key = (resource_id, track_id, quality_id, slice_id)

        init = init_segs.get(key)

        if init != None:
            return init

        init = self.storage.get_init_segment(
            resource_id=resource_id,
            track_id=track_id,
            quality_id=quality_id,
            slice_id=slice_id,
        )

        init_segs[key] = init
        return init

    def _glue_mts_list(self, slice_, index_sorted_mts_list):

        segs = []
        init_segs = {}

        for mts in index_sorted_mts_list:

            hs = {}
            es_list = []

            duration = 0
            for s in mts:
                init = self._get_init_seg(
                    init_segs,
                    resource_id=slice_['resource_id'],
                    track_id=s['track_id'],
                    quality_id=s['quality_id'],
                    slice_id=slice_['slice_id'],
                )

                es = "{}:{}_{}:{}".format(s['track_id'], init['segment_id'], s['segment_id'], s['time'] + 10000000)
                es_list.append(es)
                duration = max(s['duration'], duration)

            sid = ",".join(es_list)
            hs['url'] = "/v0/segs/ts/standard/none/{}.ts".format(sid)
            hs['duration'] = duration

            segs.append(hs)

        return segs

    def vod(self, resource_id, sid):
        raw_segments = self.storage.get_segments(resource_id)

        slice_ = self.storage.get_slice(resource_id, raw_segments[0]['slice_id'])

        qid_to_sid = self._make_qid_tp_sid(slice_)

        mts_list_per_q = collections.defaultdict(lambda: collections.defaultdict(list))

        for s in raw_segments:
            sid = qid_to_sid[s['quality_id']]

            mts_list_per_q[sid][s['index']].append(s)

        assert sid in mts_list_per_q, "failed to find stream {} in resource {}, map: {}".format(sid, resource_id, qid_to_sid)

        index_sorted_mts_list = sorted(mts_list_per_q[sid].values(), key=lambda t: t[0]['index'])

        segs = self._glue_mts_list(slice_, index_sorted_mts_list)

        max_duration = max([x['duration'] for x in segs])

        tmpl_path = os.path.dirname(os.path.realpath(__file__)) + '/templates/hlsv3_vod.mako'
        tmpl = open(tmpl_path).read()
        r = Template(tmpl).render(segs=segs, max_duration=max_duration)
        return str(r)

class SSPlaylistBuilder(object):

    four_cc = {
        "h264": "H264",
        "aac": "AACL",
        "ac3": "EC-3",
        "eac3": "EC-3",
    }

    audio_tag = {
        "aac": 255,
        "eac3": 65534,
        "ac3": 65534,
    }

    time_scale = 10000000
    default_live_delay = 10
    regex = re.compile(r'^/v(?P<version>\d+)/pl/(?P<protocol>ss)/(?P<dialect>\w+)/(?P<drm>\w+)/(?P<resource_id>[\w_/-]+)\.ism(?P<is_live>l?)/(manifest)?$')

    def __init__(self, storage):
        self.storage = storage

    def on_get(self, req, resp, dialect, drm, resource_id, suffix):

        assert suffix in ['isml', 'ism']

        manifest_type = 'vod'
        if suffix == 'isml':
            manifest_type = 'live'

        root = ET.Element("SmoothStreamingMedia", MajorVersion="2", MinorVersion="2", TimeScale=str(self.time_scale))

        if manifest_type == 'live':
            self.live(root, resource_id, req)
        elif manifest_type == 'vod':
            self.vod(root, resource_id, req)
        else:
            raise Exception("Wrong manifest_type")

        resp.body = '<?xml version="1.0" encoding="utf-8"?>\n' + ET.tostring(root, method="xml", encoding='utf-8')
        resp.content_type = "application/vnd.ms-sstr+xml"

    def live(self, root, resource_id, req):

        root.set("IsLive", "TRUE")
        root.set("LookAheadFragmentCount", "2")
        # root.set("DVRWindowLength", "3000000000")

        time_shift = req.get_param_as_int('time_shift') or 0

        start_time = 0 if req.get_param_as_bool('start_from_null', blank_as_true=True) else long((time.time() - self.default_live_delay) * self.time_scale) - long(time_shift)

        segments_num = (req.get_param_as_int("segments_num") or 5) * 2
        raw_segments = self.storage.get_raw_segments(resource_id, start_time=long(start_time), limit=segments_num)

        self.fill_manifest(root, resource_id, raw_segments)
        root.set("Duration", "0")

    def vod(self, root, resource_id, req):
        start_time = req.get_param_as_int("start_time")
        end_time = req.get_param_as_int("end_time")
        raw_segments = self.storage.get_raw_segments(resource_id, start_time=start_time, end_time=end_time)
        self.fill_manifest(root, resource_id, raw_segments)

    def fill_manifest(self, root, resource_id, raw_segments):
        # raw_segments format: list of
        # { "_id" : { "time" : (time), "tid" : (track_id) },
        #   "d" : (duration),
        #   "s": (slice_id),
        #   "ids" : [ { "id" : (segment_id), "qid" : (quality_id) } ] }
        if not raw_segments:
            root.set("Duration", "0")
            return

        slice_ = self.storage.get_slice(resource_id, raw_segments[0]['s'])
        segments = {t['track_id']: list() for t in slice_['track_list']}

        for s in raw_segments:
            segments[s["_id"]["tid"]].append({"t": s["_id"]["time"], "d": s["d"]}) # , "i": {x["qid"]:x["id"] for x in s["ids"]}})

        max_duration = 0

        for track in slice_['track_list']:
            track_element = ET.SubElement(root, "StreamIndex", Name='track%s' % str(int(track['track_id'])), Type=track['media_type'], Url="QualityLevels({bitrate})/Fragments(track%s={start time})" % str(int(track['track_id'])))

            self.fill_qualities(track_element, track)
            duration = 0
            for s in segments[track['track_id']]:
                duration += s['d']
                ET.SubElement(track_element, "c", t=str(int(s['t']))) #d=str(int(s['d']))
            max_duration = max(max_duration, duration)

        root.set("Duration", str(max_duration))

    def fill_qualities(self, stream_index, track):
        for index, q in enumerate(track['quality_list']):
            quality_element = ET.SubElement(stream_index, "QualityLevel", Bitrate=str(q['bitrate']['average']), Index=str(index), CodecPrivateData=q['codec_private_data'])
            if track['media_type'] == 'video':
                quality_element.set("FourCC", self.four_cc[q['codec']])
                quality_element.set("MaxWidth", str(int(q['width'])))
                quality_element.set("MaxHeight", str(int(q['height'])))
            elif track['media_type'] == 'audio':
                quality_element.set("FourCC", self.four_cc[q['codec']])
                quality_element.set("SamplingRate", str(int(q['sampling_rate'])))
                quality_element.set("AudioTag", str(self.audio_tag[q['codec']]))
                quality_element.set("Channels", str(q['channels']))
                quality_element.set("BitsPerSample", str(q['sample_bit_depth']))
                quality_element.set("PacketSize", "1")

