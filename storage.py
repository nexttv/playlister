# -*- coding: utf-8 -*-

import pymongo
import os
from bson.son import SON
# import rados

class MongoStorage(object):
    def __init__(self):
        self.mongo = pymongo.MongoClient(
            host=os.environ.get("MONGO_CONN_STR", "127.0.0.1"),
            max_pool_size=10)
        # self.mongo.db.segments.ensureIndex()
        # self.mongo.db.slices.ensureIndex()

    def get_slice(self, resource_id, slice_id):
        return self.mongo.db.slices.find_one({"resource_id": resource_id, "slice_id": slice_id})

    # def get_slices(self, resource_id, start_time = None, end_time = None):
    #     query = {"resource_id": resource_id}
    #     if end_time:
    #         query['start_time'] = {'$lt': end_time}
    #     return self.mongo.db.slices.find(query)

    def get_segments(self, resource_id, track_id=None, quality_id=None, start_time=None, end_time=None):
        query = {"resource_id": resource_id, "type": "data"}
        if track_id:
            query['track_id'] = track_id
        if quality_id:
            query['quality_id'] = quality_id
        if start_time or end_time:
            query['time'] = dict()
        if start_time:
            query['time']['$gte'] = start_time
        if end_time:
            query['time']['$lt'] = end_time

        return self.mongo.db.segments.find(query).sort('index', pymongo.ASCENDING)
        # sort by media_type V, track_id V, quality_id V

    def get_raw_segments(self, resource_id, start_time=None, end_time=None, limit=None):
        query = {"resource_id": resource_id, "type": "data"}
        if start_time or end_time:
            query['time'] = dict()
        if start_time:
            query['time']['$gte'] = start_time
        if end_time:
            query['time']['$lt'] = end_time

        aggregate_query = [
            {"$match": query},
            {"$group":
                {
                    "_id": {"time": "$time", "tid": "$track_id"},
                    "d": {"$first": "$duration"},
                    "s": {"$first": "$slice_id"}
                    # ,
                    # "ids": {"$push":
                    #     {"id": "$segment_id", "qid": "$quality_id"}}
                }
            },
            {"$sort": SON([("_id.time", 1)])}]

        if limit:
            aggregate_query.append({"$limit": limit})
        return self.mongo.db.segments.aggregate(aggregate_query)['result']

    def get_init_segment(self, resource_id, track_id, quality_id, slice_id):
        query = {"resource_id": resource_id, 'track_id': track_id, 'quality_id':quality_id, 'type':'init', 'slice_id': slice_id}
        return self.mongo.db.segments.find_one(query)

# class CephStorage(object):
#     def __init__(self, config):
#         self.ceph = rados.Rados(conffile=str(config['ceph_conffile']), conf = dict (keyring = str(config['ceph_keyring'])))
#         self.ceph.connect()
#         if not self.ceph.pool_exists('segments'):
#             self.ceph.create_pool('segments')
#         self.rados_ioctx = self.ceph.open_ioctx('segments')

#     def get_segment(self, segment_id):
#         s = self.rados_ioctx.stat(segment_id)
#         return self.rados_ioctx.read(segment_id, s[0])

#     def close(self):
#         self.rados_ioctx.close()
#         self.ceph.shutdown()
